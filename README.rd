= Apache Arrowフォーマットはなぜ速いのか

2020年代、ビッグデータをどう扱えばよいか。今は各プロダクト毎に効率的な扱い方を実装していますが、2020年代はそんな時代ではありません！ビッグデータの扱いでも、共通で必要なものはプロダクトを超えて協力して開発して共有する、そんな時代です！ビッグデータのための共通基盤、それがオープンソースのApache Arrowです。AmazonもGoogleもNVIDIAも開発に参加しています。

このセッションではApache Arrow開発チームの主要メンバーがApache Arrowフォーマットがなぜ速いのかを説明します。

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者：須藤功平

==== 株式会社クリアコードのロゴ

CC BY-SA 4.0

原著作者：株式会社クリアコード

ページヘッダーで使っています。

==== Apache Arrowのロゴ

Apache License 2.0

原著作者：The Apache Software Foundation

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-db-tech-showcase-online-2020

=== 表示

  rabbit rabbit-slide-kou-db-tech-showcase-online-2020.gem

